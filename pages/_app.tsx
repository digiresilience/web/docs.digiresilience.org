import CssBaseline from "@mui/material/CssBaseline";
import Head from "next/head";
import Favicon from "images/favicon.ico";
import { AppProvider } from "../components/common/AppProvider";
import "../styles/global.css";
import "@fontsource/roboto/400.css";
import "@fontsource/poppins/400.css";
import "@fontsource/poppins/700.css";
import "@fontsource/playfair-display/900.css";

const CDRDocsWeb = ({ Component, pageProps }) => (
  <>
    <Head>
      <link rel="icon" type="image/png" href={Favicon.src} />
    </Head>
    <CssBaseline />
    <AppProvider>
      <Component {...pageProps} />
    </AppProvider>
  </>
);

export default CDRDocsWeb;
