import Head from "next/head";
import { getDocsProps } from "lib/helpers";
import { Header } from "../components/home/Header";
import { Layout } from "../components/common/Layout";

const Home = () => (
  <Layout>
    <Head>
      <title>Center for Digital Resilience</title>
    </Head>
    <Header />
  </Layout>
);

export default Home;

export async function getStaticProps() {
  return getDocsProps();
}
