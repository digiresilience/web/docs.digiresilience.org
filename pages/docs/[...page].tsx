import Head from "next/head";
// import { useTranslate } from "react-polyglot";
import { Container } from "@mui/material";
import { GetStaticPropsContext } from "next";
import { Client } from "@notionhq/client";
import { Page as PageBody } from "components/docs/Page";
import { Layout } from "components/common/Layout";
import { getNotionProperty, getDocsProps } from "lib/helpers";

// const t = useTranslate();
const Page = ({ page }) => (
  <Layout>
    <Head>
      <title>CDR Docs</title>
    </Head>
    <Container maxWidth="md" sx={{ mb: 5 }}>
      <PageBody page={page} />
    </Container>
  </Layout>
);

export async function getStaticPaths() {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const databaseID = process.env.NOTION_DOCS_DATABASE_ID;

  const response = await notion.databases.query({
    database_id: databaseID,
    filter: {
      property: "Published",
      checkbox: {
        equals: true,
      },
    },
  });

  const paths = await Promise.all(
    response.results.map(async (result: any) => {
      const properties: any = {};
      for await (const key of Object.keys(result.properties)) {
        const val = result.properties[key];
        const prop = await getNotionProperty(notion, result.id, val.id);
        properties[key] = prop;
      }

      return {
        params: {
          page: properties.Path[0].rich_text.plain_text.split("/"),
        },
      };
    })
  );

  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps(context: GetStaticPropsContext) {
  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const databaseID = process.env.NOTION_DOCS_DATABASE_ID;
  const response = await notion.databases.query({
    database_id: databaseID,
    filter: {
      and: [
        {
          property: "Published",
          checkbox: {
            equals: true,
          },
        },
        {
          property: "Path",
          rich_text: {
            equals: (context.params.page as any[]).join("/"),
          },
        },
      ],
    },
  });

  const pages = await Promise.all(
    response.results.map(async (result: any) => {
      const blockRequest = await notion.blocks.children.list({
        block_id: result.id,
        page_size: 50,
      });

      const properties: any = {};

      for await (const key of Object.keys(result.properties)) {
        const val = result.properties[key];
        const prop = await getNotionProperty(notion, result.id, val.id);
        properties[key] = prop;
      }

      return {
        id: result.id,
        title: properties.Name[0].title.plain_text,
        text: blockRequest.results,
      };
    })
  );

  const docsProps = await getDocsProps();

  return {
    props: { page: pages[0], docs: docsProps.props.docs },
  };
}

export default Page;
