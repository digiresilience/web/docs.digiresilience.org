import {
  FC,
  PropsWithChildren,
  createContext,
  useContext,
  useMemo,
} from "react";
import { useTheme } from "@mui/material/styles";
import { useMediaQuery } from "@mui/material";
import { colors, typography } from "styles/theme";

const loader = (image: any) => `${image.src}?${image.width ?? ""}`;

const useMobile = (): boolean => {
  const theme = useTheme();
  return useMediaQuery(theme.breakpoints.down("sm"));
};

const useTablet = (): boolean => {
  const theme = useTheme();
  return useMediaQuery(theme.breakpoints.down("lg"));
};

const AppContext = createContext({
  colors,
  typography,
  loader,
  useMobile,
  useTablet,
});

export const AppProvider: FC<PropsWithChildren> = ({ children }) => {
  const values = useMemo(
    () => ({
      colors,
      typography,
      loader,
      useMobile,
      useTablet,
    }),
    []
  );
  return <AppContext.Provider value={values}>{children}</AppContext.Provider>;
};

export function useAppContext() {
  return useContext(AppContext);
}
