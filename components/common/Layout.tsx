import { FC, PropsWithChildren } from "react";
import { Grid } from "@mui/material";
import { useAppContext } from "./AppProvider";

export const Layout: FC<PropsWithChildren> = ({ children }) => {
  const { useMobile } = useAppContext();
  const mobile = useMobile();

  return (
    <Grid container direction="column">
      <Grid item sx={{ mt: mobile ? "90px" : "120px" }}>
        {children}
      </Grid>
    </Grid>
  );
};
