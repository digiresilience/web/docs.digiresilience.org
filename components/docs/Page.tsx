import { FC } from "react";
import { Card, Grid, Box } from "@mui/material";
import { Render as RenderNotion } from "@9gustin/react-notion-render";
import { useAppContext } from "../common/AppProvider";

interface PageProps {
  page: {
    id: string;
    title: string;
    text: any;
  };
}

export const Page: FC<PageProps> = ({ page: { id, title, text } }) => {
  const {
    colors: { white },
    typography: { h3, body },
  } = useAppContext();

  return (
    <Card key={id} sx={{ backgroundColor: white, padding: 3 }}>
      <Grid item container direction="column">
        <Grid item>
          <Box component="h3" sx={h3}>
            {title}
          </Box>
        </Grid>
        <Grid item>
          <Box component="body" sx={body}>
            <RenderNotion blocks={text} classNames />
          </Box>
        </Grid>
      </Grid>
    </Card>
  );
};
