import { Client } from "@notionhq/client";
import cachedDocs from "./docs.json";

export const getNotionProperty = async (
  client: any,
  pageID: string,
  propertyID: string
) => {
  const propertyItem = await client.pages.properties.retrieve({
    page_id: pageID,
    property_id: propertyID,
  });
  if (propertyItem.object === "property_item") {
    return [propertyItem];
  }

  let nextCursor = propertyItem.next_cursor;
  const { results } = propertyItem;

  while (nextCursor !== null) {
    const propItem = await client.pages.properties.retrieve({
      page_id: pageID,
      property_id: propertyID,
      start_cursor: nextCursor,
    });

    nextCursor = propItem.next_cursor;
    results.push(...propItem.results);
  }

  return results;
};

export async function getDocsProps() {
  if (cachedDocs) {
    return {
      props: { docs: cachedDocs },
    };
  }

  const notion = new Client({ auth: process.env.NOTION_API_KEY });
  const databaseID = process.env.NOTION_DOCS_DATABASE_ID;
  const response = await notion.databases.query({
    database_id: databaseID,
    filter: {
      and: [
        {
          property: "Published",
          checkbox: {
            equals: true,
          },
        },
      ],
    },
  });

  const docs = await Promise.all(
    response.results.map(async (result: any) => {
      const properties: any = {};

      for await (const key of Object.keys(result.properties)) {
        const val = result.properties[key];
        const prop = await getNotionProperty(notion, result.id, val.id);
        properties[key] = prop;
      }

      return {
        id: result.id,
        section: properties.Section[0].rich_text.plain_text,
        title: properties.Name[0].title.plain_text,
        path: properties.Path[0].rich_text.plain_text,
        position: properties.Position[0].number,
      };
    })
  );

  return {
    props: { docs },
  };
}
