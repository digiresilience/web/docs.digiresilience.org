const withMDX = require("@next/mdx")({
  extension: /\.mdx?$/,
});

module.exports = withMDX({
  reactStrictMode: true,
  images: {
    loader: "custom",
  },
  trailingSlash: true,
  pageExtensions: ["js", "jsx", "ts", "tsx", "mdx"],
});
